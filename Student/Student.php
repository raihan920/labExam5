<?php
include_once ('../Message/Message.php');
include_once ('../Utility/Utility.php');
class Student{
    public $id="";
    public $firstname="";
    public $middlename="";
    public $lastname="";
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","labxm5b22");
    }

    public function prepare($data)
    {
        if(array_key_exists('firstname',$data)){
            $this->firstname=$data['firstname'];
        }
        if(array_key_exists('middlename',$data)){
            $this->middlename=$data['middlename'];
        }
        if(array_key_exists('lastname',$data)){
            $this->lastname=$data['lastname'];
        }
    }

    public function create()
    {
        $query = "INSERT INTO `labxm5b22`.`studentinfo` (`firstname`, `middlename`, `lastname`) VALUES ('".$this->firstname."', '".$this->middlename."', '".$this->lastname."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
            <div>
                <strong>Success!!!</strong>Data has been stored successfully;
            </div>
            ");
            Utility::redirect('index.php');
        }else{
            echo "Error!!";
        }

    }

    public function index()
    {
        $allStudent = Array();
        $query = "SELECT * FROM `labxm5b22`.`studentinfo`";
        $result = mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $allStudent[]=$row;
        }
        return $allStudent;
    }

    public function delete()
    {
        $query = "DELETE FROM `labxm5b22`.`studentinfo` WHERE `id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
            <div>
                <strong>Deleted!!!</strong>Data has been deleted successfully;
            </div>
            ");
            Utility::redirect('index.php');
        }else{
            echo "Error!!";
        }

    }

    public function update()
    {
        $query = "UPDATE `labxm5b22`.`studentinfo` SET `firstname` = '".$this->firstname."', `middlename` = '".$this->middlename."', `lastname` = '".$this->lastname."' WHERE `studentinfo`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
            <div>
                <strong>Updated!!!</strong>Data has been updated successfully;
            </div>
            ");
            Utility::redirect('index.php');
        }else{
            echo "Error!!";
        }

    }

    public function view()
    {
        $query = "SELECT * FROM `labxm5b22`.`studentinfo` WHERE `id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_object($result);
        return $row;
    }
}