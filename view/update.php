<?php
include_once('../Student/Student.php');
include_once('../Message/Message.php');
include_once('../Utility/Utility.php');

$student = new Student();
$student->prepare($_POST);
$student->update();