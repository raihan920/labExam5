<?php
include_once ('../Student/Student.php');
include_once ('../Message/Message.php');
include_once ('../Utility/Utility.php');
session_start();

$student = new Student();
$allStudents = $student->index();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>index page</title>
</head>
<body>
    <a href="create.php" type="button">Create New</a>
    <div id="message">
        <?php
        if(array_key_exists('message',$_SESSION)&&(!empty($_SESSION['message']))){
            echo Message::message();
        }
        ?>
    </div>
    <table>
        <thead>
            <tr>
                <td>#</td>
                <td>ID</td>
                <td>First Name</td>
                <td>Middle Name</td>
                <td>Last Name</td>
                <td>Action</td>
            </tr>

        </thead>
        <tbody>
        <?php
        $sl = 0;
        foreach($allStudents as $student){
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl;?></td>
                <td><?php echo $student->id?></td>
                <td><?php echo $student->firstname?></td>
                <td><?php echo $student->middlename?></td>
                <td><?php echo $student->lastname?></td>
                <td>
                    <a href="edit.php?id=<?php echo $student->id?>">Edit</a>
                    <a href="delete.php?id=<?php echo $student->id?>" id="delete" Onclick="return ConfirmDelete()">Delete</a>
                    <a href="view.php?id=<?php echo $student->id?>">View</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</body>
<script>
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if(x){
            return true;
        }else {
            return false;
        }

    }
</script>
</html>
