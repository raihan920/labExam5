<?php
include_once('../Student/Student.php');
include_once('../Message/Message.php');
include_once('../Utility/Utility.php');

$student = new Student();
$student->prepare($_GET);
$student->view();
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>index page</title>
</head>
<body>
<a href="create.php">Create New</a>
<div id="message">
</div>
<table>
    <thead>
    <tr>
        <td>#</td>
        <td>ID</td>
        <td>First Name</td>
        <td>Middle Name</td>
        <td>Last Name</td>
        <td>Action</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php echo $this->id?></td>
        <td><?php echo $this->firstname?></td>
        <td><?php echo $this->middlename?></td>
        <td><?php echo $this->lastname?></td>
    </tr>
    </tbody>
</table>
</body>
</html>